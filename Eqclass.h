#ifndef _EQCLASS_H
#define _EQCLASS_H

#include <iostream>
#include <stdio.h>
#include <errno.h>
#include "List.h"
#include "Itemset.h"

class EqGrNode;

class Eqclass {
  private:
    List<Itemset *> *theList;
  public:
    Eqclass(){
      theList = new List<Itemset *>;
      if (theList == NULL){
        perror("memory :: Eqclass");
        exit(errno);
      }
    }
    Eqclass(List<Itemset *> *ll){
      theList = ll;
    }

    ~Eqclass(){
      if (theList){
        theList->clear();
        delete theList;
      }
      theList = NULL;
    }

    inline List<Itemset *> * list() {
      return theList;
    }
    inline void set_list(List<Itemset *> * ll) {
      theList = ll;
    }
    inline void append(Itemset *it) {
      theList->append(it);
    }

    inline void remove(ListNode<Itemset *> *prev, ListNode<Itemset *> *it) {
      theList->remove(prev, it);
    }

    inline void sortedAscend(Itemset *it, CMP_FUNC func) {
      theList->sortedAscend(it, func);
    }
    inline void sortedDescend(Itemset *it, CMP_FUNC func) {
      theList->sortedDescend(it, func);
    }

    Itemset * uniqsorted(Itemset *it, CMP_FUNC func) {
      Itemset *rval;
      if (!(rval = theList->find(it, Itemset::Itemcompare))){
        theList->sortedAscend(it, func);
    }
    return rval;
}

int subseq(Itemset *it) {
  ListNode<Itemset *> *hd = theList->head();
  for (;hd; hd=hd->next()){
    if (it->subsequence(hd->item())){
      return 1;
    }
  }
  return 0;
}

friend std::ostream& operator<<(std::ostream& outputStream, Eqclass& EQ) {
  outputStream << "EQ : ";
  EQ.theList->print();
  return outputStream;
};
};

class EqGrNode {
  private:
    int *theElements;
    unsigned int numElements;
    List<int *> *theCoverL;
    List<Array *> *theCliqueL;
    List<Itemset *> *theLargeL;
    int theFlg; //indicates if class is in memory
  public:
    EqGrNode(int sz)
    {
      numElements = sz;
      theElements = new int[sz];
      theCoverL = new List<int *>;
      theLargeL = new List<Itemset *>;
      theCliqueL = new List<Array *>;
      theFlg = 0;
    }

    ~EqGrNode()
    {
      delete [] theElements;
      delete theCoverL;
      delete theCliqueL;
      theElements = NULL;
      theCoverL = NULL;
      theFlg = 0;
    }

    inline int getflg()
    {
      return theFlg;
    }
    inline void setflg(int val)
    {
      theFlg=val;
    }

    inline List<Array *> *clique()
    {
      return theCliqueL;
    }

    inline List<Itemset *> *largelist()
    {
      return theLargeL;
    }

    inline List<int *> * cover()
    {
      return theCoverL;
    }
    inline int * elements()
    {
      return theElements;
    }
    inline int num_elements() {
      return numElements;
    }
    inline void add_element(int el, int pos)
    {
      theElements[pos] = el;
    }
    inline int get_element(int pos)
    {
      return theElements[pos];
    }
    inline void remove_el(int pos) {
      for (unsigned int i = pos; i < numElements-1; i++)
        theElements[i] = theElements[i+1];
      numElements--;
    }
    inline void add_cover(int *eq)
    {
      theCoverL->append(eq);
    }

    inline int find_element(Itemset *it) {
      unsigned int i, j, cnt = 0;
      for (i=0, j=0; i < numElements && j < it->size();){
        if (theElements[i] < (*it)[j]) i++;
        else if (theElements[i] == (*it)[j]){
          i++;
          j++;
          cnt++;
        }
        else return 0;
      }
      if (cnt == it->size()) return 1;
      else return 0;
    }

    inline int find_element(Array *it) {
      unsigned int i,j;
      unsigned int cnt = 0;
      for (i=0, j=0; i < numElements && j < it->size();){
        if (theElements[i] < (*it)[j]) i++;
        else if (theElements[i] == (*it)[j]){
          i++;
          j++;
          cnt++;
        }
        else return 0;
      }
      if (cnt == it->size()) return 1;
      else return 0;
    }

    inline int get_common(Array *arr, int *newcov)
    {
      unsigned int i,j;
      int cnt=0;

      for (i=0, j=0; i < numElements && j < arr->size();){
        if (theElements[i] < (*arr)[j]) i++;
        else if (theElements[i] > (*arr)[j]) j++;
        else{
          newcov[i] = 1;
          i++;
          j++;
          cnt++;
        }
      }
      return cnt;
    }

    friend std::ostream& operator<<(std::ostream& outputStream, EqGrNode& EQ){
      std::cout << "ELEMENTS : ";
      for (unsigned int i = 0; i < EQ.numElements; i++){
        std::cout << i << "=" << EQ.theElements[i] << " ";
      }
      std::cout << std::endl;
      ListNode<int *> *hd = EQ.theCoverL->head();
      std::cout << "COVER : ";
      for (; hd; hd=hd->next()){
        std::cout << *(hd->item()) << " ";
      }
      std::cout << std::endl;
      ListNode<Array *> *clhd = EQ.theCliqueL->head();
      std::cout << "CLIQUE : ";
      for (; clhd; clhd=clhd->next()){
        std::cout << *(clhd->item()) << ", ";
      }
      std::cout << std::endl;

      return outputStream;
    };

};

extern void eq_insert(List<Eqclass *> &EQC, Itemset *it);
extern void eq_print(Eqclass *LargeEqclass);
#endif
