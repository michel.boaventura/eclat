#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#include "Eqclass.h"

void eq_print(Eqclass *LargeEqclass){
  ListNode<Itemset*> *head = LargeEqclass->list()->head();
  if (head){
    std::cout << (*head->item())[0] << ": ";
    while(head){
      std::cout << (*head->item())[1] << " ";
      head = head->next();
    }
    std::cout << std::endl;
  }
}

Eqclass * get_node(Itemset *it){
  Eqclass *node = new Eqclass();
  if (node == NULL){
    std::cout << "MEMORY EXCEEDED\n";
    exit(-1);
  }
  node->list()->append(it);
  return node;
}

void eq_insert(List<Eqclass *> &EQC, Itemset *it){
  ListNode<Eqclass *> *head = EQC.head();
  ListNode<Eqclass *> *lnode;

  if (head == NULL){
    lnode = new ListNode<Eqclass *>(get_node(it), 0);
    if (lnode == NULL){
      std::cout << "MEMORY EXCEEDED\n";
      exit(-1);
    }
    EQC.set_head(lnode);
    EQC.set_last(lnode);
  }
  else{
    int res  = it->compare(*(head->item()->list()->head()->item()),it->size()-1);
    if (res == 0){
      head->item()->sortedAscend(it, Itemset::Itemcompare);
    }
    else if (res < 0){
      lnode = new ListNode<Eqclass *>(get_node(it),0);
      if (lnode == NULL){
        std::cout << "MEMORY EXCEEDED\n";
        exit(-1);
      }
      lnode->set_next(EQC.head());
      EQC.set_head(lnode);
    }
    else{
      for (;head->next(); head = head->next()){
        res  = it->compare(*(head->next()->item()->list()->head()->item()),
            it->size()-1);
        if (res == 0){
          head->next()->item()->sortedAscend(it, Itemset::Itemcompare);
          return;
        }
        else if ( res < 0){
          lnode = new ListNode<Eqclass *>(get_node(it),0);
          if (lnode == NULL){
            std::cout << "MEMORY EXCEEDED\n";
            exit(-1);
          }
          lnode->set_next(head->next());
          head->set_next(lnode);
          return;
        }
      }
      lnode = new ListNode<Eqclass *>(get_node(it),0);
      if (lnode == NULL){
        std::cout << "MEMORY EXCEEDED\n";
        exit(-1);
      }
      EQC.last()->set_next(lnode);
      EQC.set_last(lnode);
    }
  }
}
