#include "List.h"

template <class Items>
void ListNode<Items>::clear(){
  theNext = NULL;
  delete theItem;
}

void ListNode<int>::clear(){
  theNext = NULL;
  theItem = 0;
}

template <class Items>
List<Items>::List() {
  theHead = 0;
  theLast = 0;
  theSize = 0;
}

template <class Items>
List<Items>::~List() {
  clear();
}

template <class Items>
void List<Items>::clear() {
  ListNode<Items> *node = theHead;

  while (node){
    theHead = theHead->theNext;
    node->clear();
    delete node;
    node = theHead;
  }
  theHead = NULL;
  theLast = NULL;
  theSize = 0;
}

  template <class Items>
void List<Items>::remove(ListNode<Items> * prev, ListNode<Items> * val)
{
  if (prev == NULL) theHead = val->theNext;
  else prev->theNext = val->theNext;
  theSize--;
}

  template <class Items>
void List<Items>::remove_ascend(Items val, CMP_FUNC cmpare)
{
  ListNode<Items> *prev = NULL;
  if (find_ascend(prev, val, cmpare)){
    if (prev == NULL) remove (prev, theHead);
    else remove (prev, prev->theNext);
  }
}


  template <class Items>
int List<Items>::find_ascend(ListNode<Items> *&prev,
    Items item, CMP_FUNC cmpare)
{
  ListNode<Items> *temp = theHead;
  if (theHead == 0) return 0;
  else{
    int res = cmpare((void *)item,(void *)theHead->theItem);
    if (res == 0) return 1;
    else if (res < 0) return 0;
    else{
      while (temp->theNext){
        res = cmpare((void *)item,(void *)temp->theNext->theItem);
        if (res < 0){
          prev = temp;
          return 0;
        }
        else if (res == 0){
          prev = temp;
          return 1;
        }
        temp = temp->theNext;
      }
      prev = theLast;
      return 0;
    }
  }
}
//find_descend(ListNode<Items> *&, Items, CMP_FUNC);


  template <class Items>
void List<Items>::append (Items item)
{
  ListNode<Items> *node;

  theSize++;
  node = new (ListNode<Items>);
  if (node == NULL){
    cout << "MEMORY EXCEEDED\n";
    exit(-1);
  }
  node->theItem = item;
  node-> theNext = 0;

  if (theHead == 0){
    theHead = node;
    theLast = node;
  }
  else{
    theLast->theNext = node;
    theLast = node;
  }
}

template <class Items>
ListNode<Items> *List<Items>::node(int pos){
  ListNode<Items> *head = theHead;
  for (int i=0; i < pos && head; head = head->theNext,i++);
  return head;
}


  template <class Items>
void List<Items>::sortedAscend (Items item, CMP_FUNC cmpare)
{
  ListNode<Items> *node;
  ListNode<Items> *prev = NULL;

  if (!find_ascend(prev, item, cmpare)){
    node = new (ListNode<Items>);
    if (node == NULL){
      cout << "MEMORY EXCEEDED\n";
      exit(-1);
    }
    node->theItem = item;
    node->theNext = 0;
    theSize++;

    if (theHead == 0){
      theHead = node;
      theLast = node;
    }
    else if (prev == NULL){
      node->theNext = theHead;
      theHead = node;
    }
    else{
      node->theNext = prev->theNext;
      prev->theNext = node;
      if (prev == theLast) theLast = node;
    }
  }
}

  template <class Items>
void List<Items>::sortedDescend (Items item, CMP_FUNC cmpare)
{
  ListNode<Items> *node;
  ListNode<Items> *temp = theHead;

  /*printf("theSize %d\b", theSize);*/
  theSize++;
  node = new ListNode<Items>;
  if (node == NULL){
    cout << "MEMORY EXCEEDED\n";
    exit(-1);
  }
  node->theItem = item;
  node->theNext = 0;

  if (theHead == 0){
    theHead = node;
    theLast = node;
  }
  else if (cmpare((void *)item,(void *)theHead->theItem) > 0){
    node->theNext = theHead;
    theHead = node;
  }
  else{
    while (temp->theNext){
      if (cmpare((void *)item,(void *)temp->theNext->theItem) > 0){
        node->theNext = temp->theNext;
        temp->theNext = node;
        return;
      }
      temp = temp->theNext;
    }
    theLast->theNext = node;
    theLast = node;
  }

}

void List<int>::print (){
  cout << "LIST: ";
  if (!(theHead == NULL)){
    ListNode<int> *head = theHead;
    for (; head; head = head->theNext){
      cout << head->theItem << " ";
    }
    cout << "\n";
  }
}

template <class Items>
void List<Items>::print (){
  cout << "LIST: ";
  if (!(theHead == NULL)){
    ListNode<Items> *head = theHead;
    for (; head; head = head->theNext){
      cout << *(head->theItem) << " ";
    }
  }
}

template <class Items>
Items List<Items>::Frontremove (){
  ListNode<Items> *node = theHead;
  if (theHead){
    Items it = theHead->theItem;
    theHead = node->theNext;
    delete node;
    return it;
  }
  else return NULL;
}

