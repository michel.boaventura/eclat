#ifndef __ARRAY_H
#define __ARRAY_H
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <iostream>
#include <sys/types.h>
//#include <malloc.h>

class Array {
public:
   int *theArray;
   unsigned int theSize;
   unsigned int totSize;

   //Array (int sz, int incr);
   Array(int sz, int *ary);
   Array(int sz);
   ~Array();

   inline unsigned int size();
   inline void add (int);
   int subsequence(Array * ar);
   //inline void add (int, unsigned int);
   inline void add_ext(int val, int off, int *ary)
   {
      ary[off+theSize] = val;
      theSize++;
   }

   inline int item (unsigned int);

   int operator [] (unsigned int index)
   {
      return theArray[index];
   };

   inline void setitem(int pos, int val){
      theArray[pos] = val;
   };

   inline int totsize()
   {
      return totSize;
   }
   inline void set_totsize(int sz){
      totSize = sz;
   }
   inline void set_size(int sz){
      theSize = sz;
   }
   inline void reset()
   {
      theSize = 0;
   }

   inline int *get_array()
   {
      return theArray;
   }
   inline void set_array(int *ary){
      theArray = ary;
   }
   //int subsequence(Array&);
   //int compare(Array&);
   friend std::ostream& operator << (std::ostream& outputStream, Array& arr);
   static int Arraycompare(void * iset1, void *iset2)
   {
      Array *it1 = (Array *) iset1;
      Array *it2 = (Array *) iset2;
      return it1->compare(*it2);
   }
   int compare(Array& ar2);
};

inline int Array::item (unsigned int index)
{
   return theArray[index];
}

inline unsigned int Array::size()
{
   return theSize;
}

inline void Array::add (int item)
{
   theArray[theSize] = item;
   theSize++;
}

#endif //__ARRAY_H
