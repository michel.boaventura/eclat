#ifndef __ITEMSET_H
#define __ITEMSET_H
#include <iostream>
#include <stdio.h>
#include <errno.h>
#include "Array.h"
#include "GArray.h"
#include "List.h"

#define SETBIT(a,b) ((a) |= (1 << (b)))
#define UNSETBIT(a,b) ((a) &= ~(1 << (b)))
#define GETBIT(a,b) ((a) & (1 << (b)))

class Itemset{
  public:
    Array *theItemset;
    GArray<int> *theTidlist;
    int theSupport;
    int theDiff;
    Itemset **theSubsets;
    int theSubsetcnt;
    int theMaxflag;

    Itemset(int it_sz, int list_sz);
    ~Itemset();

    friend std::ostream& operator<<(std::ostream& outputStream, Itemset& itemset);
    void intersect_neighbors(Itemset *it1, Itemset *it2);
    int compare(Itemset& ar2, int len);
    int compare(Itemset& ar2);
    int compare(Array& ar2, int len);
    int compare(Itemset& ar2, int len, unsigned int);
    int subsequence(Itemset * ar);

    int isetsize(){
      if (theItemset) return theItemset->size();
      else return 0;
    }

    int operator [] (int pos){
      return (*theItemset)[pos];
    };

    inline int item(int pos){
      return (*theItemset)[pos];
    };

    int litem(){
      return (*theItemset)[theItemset->size()-1];
    }

    inline void setitem(int pos, int val){
      theItemset->setitem(pos, val);
    };

    inline int tid(int pos){
      return (*theTidlist)[pos];
    };

    inline int get_max(){
      return theMaxflag;
    };

    inline void set_max(int val){
      theMaxflag = val;
    }

    inline void set_itemset (Array *ary)
    {
      theItemset = ary;
    }

    inline Array * itemset(){
      return theItemset;
    };

    inline GArray<int> *& tidlist(){
      return theTidlist;
    };

    inline Itemset ** subsets(){
      return theSubsets;
    };

    inline int num_subsets(){
      return theSubsetcnt;
    };

    void add_subset(Itemset *it){
      theSubsets[theSubsetcnt++] = it;
    };

    void add_item(int val){
      theItemset->add(val);
    };

    inline void add_tid(int val){
      theTidlist->optadd(val);
    };

    inline unsigned size(){
      return theItemset->size();
    };

    inline unsigned tidsize(){
      return theTidlist->size();
    };

    int &diff(){ return theDiff;}

    int & support(){
      return theSupport;
    };

    inline void set_support(int sup)
    {
      theSupport = sup;
    }

    inline void increment_support(){
      theSupport++;
    };

    static int intcmp (void *it1, void *it2)
    {
      int i1 = *(int *) it1;
      int i2 = *(int *) it2;
      //printf("cmp %d %d\n", i1->theSupport,
      if (i1 > i2) return 1;
      else if (i1 < i2) return -1;
      else return 0;
    }

    static int supportcmp(void *it1, void *it2) {
      Itemset * i1 = (Itemset *)it1;
      Itemset * i2 = (Itemset *)it2;
      //printf("cmp %d %d\n", i1->theSupport,
      if (i1->theSupport > i2->theSupport) return 1;
      else if (i1->theSupport < i2->theSupport) return -1;
      else return 0;
    }

    static int Itemcompare(void * iset1, void *iset2) {
      Itemset *it1 = (Itemset *) iset1;
      Itemset *it2 = (Itemset *) iset2;
      return it1->compare(*it2);
    }
};

#endif
