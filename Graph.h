#ifndef _GRAPH_H
#define _GRAPH_H

#include "GArray.h"
#include "Itemset.h"
#include "assoc.h"

class GrItem{
  private:
    unsigned int it;
    unsigned int itsup;
  public:
    GrItem(int itt=0, int ittsup=0):it(itt), itsup(ittsup){};
    unsigned& adj(){ return it; }
    unsigned & sup(){ return itsup; }
    static int cmp_grit(const void *a, const void *b);
    friend std::ostream& operator << (std::ostream& fout, GrItem git);
};

class GrNode: public GArray<GrItem *>{
  private:
    int theItem;
    unsigned theItemSup;
    long theSupSum;
  public:
    GrNode(int sz=0):GArray<GrItem *>(sz), //theSup(Array<int>(sz)),
    theItem(-1), theItemSup(0), theSupSum(0){}
    int& item(){ return theItem; }
    unsigned& sup(){ return theItemSup; }
    long & supsum(){ return theSupSum; }
    boolean find(int val);
    static int cmp_vertex(const void *a, const void *b);
};

class Graph: public GArray<GrNode *>{
  private:
  public:
    static int numF1;
    Graph(int nv):GArray<GrNode *>(nv){}
    ~Graph();
    void add_node(int item, int sup, int supsum=0);
    void add_adj(int vert, int nbr, int nbrsup);
    boolean connected(int vi, int vj);
    void sort();
    void print_iset(Itemset *it);
};

#endif //_GRAPH_H
