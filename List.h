#ifndef __LISTS_H
#define __LISTS_H

#include <iostream>
typedef int (*CMP_FUNC) (void *, void *);

template <class Items>
class ListNode {
  private:
    ListNode *theNext;
    Items     theItem;

  public:
    ListNode(Items item, ListNode *next) {
      theItem = item;
      theNext = next;
    }

    ~ListNode() {
      theNext = NULL;
      theItem = NULL;
    }

    inline ListNode<Items> *next () {
      return theNext;
    }

    inline void set_next (ListNode *next){
      theNext = next;
    }

    inline Items item(){
      return theItem;
    }

    inline void set_item(Items item){
      theItem = item;
    }
};

template <class Items>
class List {
  private:
    ListNode<Items> *theHead;
    ListNode<Items> *theLast;
    int theSize;

  public:

    List() {
      theHead = 0;
      theLast = 0;
      theSize = 0;
    };

    //only listnodes are deleted, if node->item() is a pointer to some object
    //that object is *not* deleted
    ~List() {
      ListNode<Items> *node = theHead;
      ListNode<Items> *tmp;
      while (node){
        tmp = node;
        node = node->next();
        delete tmp;
      }
      theHead = NULL;
      theLast = NULL;
      theSize = 0;
    };

    //listnodes are deleted, if node->item() is a pointer to some object
    //that object is *also*  deleted
    void clear(){
      ListNode<Items> *node = theHead;
      ListNode<Items> *tmp;
      while (node){
        tmp = node;
        node = node->next();
        delete tmp->item();
        delete tmp;
      }
      theHead = NULL;
      theLast = NULL;
      theSize = 0;
    };

    inline ListNode<Items> *head (){
      return theHead;
    };
    inline ListNode<Items> *last (){
      return theLast;
    };
    inline int size (){
      return theSize;
    };
    inline void set_head( ListNode<Items> *hd)
    {
      theHead = hd;
    }
    inline void set_last( ListNode<Items> *lst)
    {
      theLast = lst;
    }

    void append (Items item){
      ListNode<Items> *node;

      theSize++;
      node = new ListNode<Items> (item, 0);
      if (node == NULL){
        std::cout << "MEMORY EXCEEDED\n";
        exit(-1);
      }

      if (theHead == 0){
        theHead = node;
        theLast = node;
      }
      else{
        theLast->set_next(node);
        theLast = node;
      }
    };

    void remove(ListNode<Items> * prev, ListNode<Items> * val)
    {
      if (prev == NULL) theHead = val->next();
      else prev->set_next(val->next());
      if (theLast == val) theLast = prev;
      theSize--;
    }

    void sortedDescend(Items item, CMP_FUNC cmpare){
      ListNode<Items> *node;
      ListNode<Items> *temp = theHead;

      //printf("theSize %d\b", theSize);
      theSize++;
      node = new ListNode<Items>(item, 0);
      if (node == NULL){
        std::cout << "MEMORY EXCEEDED\n";
        exit(-1);
      }

      if (theHead == 0){
        theHead = node;
        theLast = node;
      }
      else if (cmpare((void *)item,(void *)theHead->item()) > 0){
        node->set_next(theHead);
        theHead = node;
      }
      else{
        while (temp->next()){
          if (cmpare((void *)item,(void *)temp->next()->item()) > 0){
            node->set_next(temp->next());
            temp->set_next(node);
            return;
          }
          temp = temp->next();
        }
        theLast->set_next(node);
        theLast = node;
      }
    };

    void sortedAscend (Items item, CMP_FUNC cmpare) {
      ListNode<Items> *node;
      ListNode<Items> *temp = theHead;

      theSize++;
      node = new ListNode<Items>(item,0);
      if (node == NULL){
        std::cout << "MEMORY EXCEEDED\n";
        exit(-1);
      }

      if (theHead == 0){
        theHead = node;
        theLast = node;
      }
      else if (cmpare((void *)item,(void *)theHead->item()) < 0){
        node->set_next(theHead);
        theHead = node;
      }
      else{
        while (temp->next()){
          if (cmpare((void *)item,(void *)temp->next()->item()) < 0){
            node->set_next(temp->next());
            temp->set_next(node);
            return;
          }
          temp = temp->next();
        }
        theLast->set_next(node);
        theLast = node;
      }
    };

    void print() { }

    void remove_ascend(Items val, CMP_FUNC cmpare);
    ListNode<Items> *node(int pos);

    Items find(Items item, CMP_FUNC cmpare) {
      ListNode<Items> *temp = theHead;
      for (;temp; temp = temp->next())
        if (cmpare((void *)item,(void *)temp->item()) == 0)
          return temp->item();
      return NULL;
    }

    int find_ascend(ListNode<Items> *&prev, Items item, CMP_FUNC cmpare) {
      ListNode<Items> *temp = theHead;
      if (theHead == 0) return 0;
      else{
        int res = cmpare((void *)item,(void *)theHead->item());
        if (res == 0) return 1;
        else if (res < 0) return 0;
        else{
          while (temp->next()){
            res = cmpare((void *)item,(void *)temp->next()->item());
            if (res < 0){
              prev = temp;
              return 0;
            }
            else if (res == 0){
              prev = temp;
              return 1;
            }
            temp = temp->next();
          }
          prev = theLast;
          return 0;
        }
      }
    }

    void prepend(Items item) {
      theSize++;
      ListNode<Items> *node = new ListNode<Items> (item, 0);
      if (node == NULL){
        std::cout << "MEMORY EXCEEDED\n";
        exit(-1);
      }
      if (theHead == NULL){
        theHead = node;
        theLast = node;
      }
      else{
        node->set_next(theHead);
        theHead = node;
      }
    }

    void insert(ListNode<Items> *&prev, Items item) {
      theSize++;
      ListNode<Items> *node = new ListNode<Items>(item,0);
      if (node == NULL){
        std::cout << "MEMORY EXCEEDED\n";
        exit(-1);
      }

      if (prev == NULL){
        theHead = node;
        theLast = node;
      }
      else{
        node->set_next(prev->next());
        prev->set_next(node);
        if (prev == theLast) theLast = node;
      }
    }
};

#endif// __LISTS_H
