CC = g++
CFLAGS = -O3 -march=native -flto -g -Wall -Wextra -Wpedantic -Wno-non-template-friend -std=c++11
SRC = .
OBJ = Itemset.o Array.o Eqclass.o HashTable.o Graph.o extl2.o \
			partition.o memman.o GArray.o Util.o calcdb.o
DEPS = $(patsubst %.o,%.h,$(OBJ))
CODE = $(patsubst %.o,%.cc,$(OBJ))

.PHONY: install clean dist-gzip dist-bzip2 dist-xz dist
.SILENT: install clean dist-gzip dist-bzip2 dist-xz dist

%.o: %.cc $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $<

eclat: $(OBJ) eclat.cc
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

assocFB: $(OBJ) assocFB.cc stats.h
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

all: eclat assocFB

clean:
	rm -f $(SRC)/*.o *~ $(SRC)/*~ eclat assocFB
